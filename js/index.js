let totalPrice = document.querySelector('.total-price');
let checkboxes = document.querySelectorAll('.cell__checkbox');
let button = document.querySelector('.add-button');

let sum = 0;

function getBlock(id) {
    let block = `<div class="table-block__row table-block__row--main table-block__row--main-${id}" data-id="${id}">
        <div class="cell justify-content-end">${id}</div>
        <div class="cell justify-content-start">
        <input type="text">
        </div>
        <div class="cell justify-content-end">
        <input class="cell__price cell__price-${id}" type="number" step="0.01">
        </div><div class="cell justify-content-start">
                    <input class="cell__checkbox" type="checkbox"></div>
        </div>`

    return block;
}

button.addEventListener('click', function () {
    let mainRows = document.querySelectorAll('.table-block__row--main')
    let lastElement = mainRows[mainRows.length - 1];
    let id = mainRows.length + 1

    lastElement.insertAdjacentHTML('afterend', getBlock(id))

})

checkboxes.forEach(function (element, index) {
    let priceInput = document.querySelector('.cell__price-' + (index + 1));

    element.addEventListener('change', function () {

        if (element.checked) {
            priceInput.classList.add('.cell__price--checked')
            priceInput.setAttribute('disabled', 'true')
            sum += Number(priceInput.value);

        } else {
            sum -= Number(priceInput.value);
            priceInput.classList.remove('.cell__price--checked');
            priceInput.removeAttribute('disabled')
        }

        totalPrice.innerText = sum;
    })
});




